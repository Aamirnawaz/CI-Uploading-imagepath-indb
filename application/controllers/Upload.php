<?php
defined('BASEPATH') OR exit('No direct script access allowed');


  
   class Upload extends CI_Controller {
	
	 private $_home_model;
      public function __construct() { 
         parent::__construct(); 
         $this->load->helper(array('form', 'url')); 

        $this->_home_model = new Home_model();
      }
		
		//step1 loading the view file
      public function index() { 
         $this->load->view('upload_form', array('error' => ' ' )); 

      } 
		
		//step2: creat function of your name and define there configuration.
      public function do_upload() { 
         $config['upload_path']   = './uploads/'; 
         $config['allowed_types'] = 'gif|jpg|png'; 
         $config['max_size']      = "10000kb"; 
         $config['max_width']     = 1400; 
         $config['max_height']    = 1200; 
         $config['overwrite'] = True;

         //step3: load the upload library and pass config variable to it 
         $this->load->library('upload', $config);
			

			//step4: checking file here creating error if file is not correct etc
         if ( ! $this->upload->do_upload('userfile')) {
            $error = array('error' => $this->upload->display_errors()); 
            $this->load->view('upload_form', $error); 
         }
			
         else { 
         	//step5: create file_info variable which store upload data in it.
         	$file_info = $this->upload->data();

         	//step6:create file_name variable which store the file name in it
   			 $file_name = $file_info['file_name'];

   			 //step 7:  pass as many as input field values to variable and then pass it to array for db storage.
         	$title = $this->input->post('title');
    		
    		//step8: pass input post valuse to array in db table field
            $data = array( 
            		'title'=>$title,
            		'image_name'=>$file_name
            	); 

            	// print_r($data);
            	
            //step9: pass array data to model
            $this->_home_model->upload_indb($data);
            redirect(upload/index);
         } 
      } 
   } 

